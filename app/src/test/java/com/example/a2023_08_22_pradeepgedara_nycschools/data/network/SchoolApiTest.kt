package com.example.a2023_08_22_pradeepgedara_nycschools.data.network

import com.google.common.truth.Truth
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class SchoolApiTest {

    private lateinit var server : MockWebServer
    private lateinit var service : SchoolApi


    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(server.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    private fun enqueueMockResponse(fileName : String){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(fileName)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun getListOfSchools_sentRequest_receivedExpected(){
        runBlocking {
            enqueueMockResponse("schoollistresponse.json")
            val responseBody = service.getListOfSchools()
            val request = server.takeRequest()
            assertThat(responseBody).isNotNull()
            Truth.assertThat(request.path).isEqualTo("/resource/s3k6-pzi2.json")
        }
    }

    @Test
    fun getSchoolInformation_sentRequest_receivedExpected(){
        runBlocking {
            enqueueMockResponse("schoolinformationresponse.json")
            val responseBody = service.getSchoolInformation()
            val request = server.takeRequest()
            assertThat(responseBody).isNotNull()
            Truth.assertThat(request.path).isEqualTo("/resource/f9bf-2cp4.json")
        }
    }



    @Test
    fun getListOFSchools_receivedResponse_checkCorrectSize(){
        runBlocking {
            enqueueMockResponse("schoollistresponse.json")
            val responseBody = service.getListOfSchools()
            val schoolsListSize = responseBody!!.size
            assertThat(schoolsListSize).isEqualTo(440)
        }
    }

    @Test
    fun getListOfSchools_receivedResponse_checkCorrectContent(){
        runBlocking {
            enqueueMockResponse("schoollistresponse.json")
            val responseBody = service.getListOfSchools()
            val firstSchool = responseBody!![0]
            assertThat(firstSchool.dbn).isEqualTo("02M260")
            assertThat(firstSchool.schoolName).isEqualTo("Clinton School Writers & Artists, M.S. 260")
            assertThat(firstSchool.phoneNumber).isEqualTo("212-524-4360")
            assertThat(firstSchool.schoolEmail).isEqualTo("admissions@theclintonschool.net")
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}