package com.example.a2023_08_22_pradeepgedara_nycschools.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SchoolListDtoItem(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val schoolName: String?,
    @SerializedName("overview_paragraph") val overView: String?,
    @SerializedName("neighborhood") val neighborhood: String?,
    @SerializedName("phone_number") val phoneNumber: String?,
    @SerializedName("fax_number") val faxNumber: String?,
    @SerializedName("school_email") val schoolEmail: String?,
    @SerializedName("website") val schoolWebsite: String?,
    @SerializedName("primary_address_line_1") val primaryAddress: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("zip") val zip: String?,
    @SerializedName("state_code") val stateCode: String?
) : Serializable {
    constructor() : this(null, null, null, null, null, null, null, null, null, null, null, null)
}
