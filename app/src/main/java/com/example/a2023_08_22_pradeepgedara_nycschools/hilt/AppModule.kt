package com.example.a2023_08_22_pradeepgedara_nycschools.hilt

import android.app.Application
import androidx.room.Room
import com.example.a2023_08_22_pradeepgedara_nycschools.data.SchoolRepository
import com.example.a2023_08_22_pradeepgedara_nycschools.data.localdb.AppDatabase
import com.example.a2023_08_22_pradeepgedara_nycschools.data.localdb.SchoolDao
import com.example.a2023_08_22_pradeepgedara_nycschools.data.network.SchoolApi
import com.example.a2023_08_22_pradeepgedara_nycschools.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSchoolRepository(api: SchoolApi, schoolDao: SchoolDao) = SchoolRepository(api, schoolDao)

    @Singleton
    @Provides
    fun provideSchoolApi(): SchoolApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolApi::class.java)
    }

    @Singleton
    @Provides
    fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "app_database")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideSchoolDao(appDatabase: AppDatabase): SchoolDao {
        return appDatabase.schoolDao()
    }
}