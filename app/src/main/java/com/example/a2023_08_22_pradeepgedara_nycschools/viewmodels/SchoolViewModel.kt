package com.example.a2023_08_22_pradeepgedara_nycschools.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2023_08_22_pradeepgedara_nycschools.data.SchoolRepository
import com.example.a2023_08_22_pradeepgedara_nycschools.data.network.DataOrException
import com.example.a2023_08_22_pradeepgedara_nycschools.models.SchoolInformationDtoItem
import com.example.a2023_08_22_pradeepgedara_nycschools.models.SchoolListDtoItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _schoolListStateFlow =
        MutableStateFlow<DataOrException<List<SchoolListDtoItem>, Boolean, Exception>>(DataOrException())
    val schoolListStateFlow: StateFlow<DataOrException<List<SchoolListDtoItem>, Boolean, Exception>>
        get() = _schoolListStateFlow

    private val _schoolInfoStateFlow =
        MutableStateFlow<DataOrException<SchoolInformationDtoItem, Boolean, Exception>>(DataOrException())

    val schoolInfoStateFlow: StateFlow<DataOrException<SchoolInformationDtoItem, Boolean, Exception>>
        get() = _schoolInfoStateFlow


    init {
        getAllSchools()
    }

    private fun getAllSchools() {
        viewModelScope.launch {
            _schoolListStateFlow.value =  repository.getAllSchools()
        }
    }

    fun getSchoolInformation(dbn : String) {
        viewModelScope.launch {
            // val result = repository.getSchoolInformation(dbn)
            _schoolInfoStateFlow.value = repository.getSchoolInformation(dbn)
        }
    }

}
