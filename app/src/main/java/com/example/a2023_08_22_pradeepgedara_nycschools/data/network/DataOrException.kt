package com.example.a2023_08_22_pradeepgedara_nycschools.data.network

class DataOrException<T, Boolean, E: Exception>(
    var data: T? = null,
    var loading: kotlin.Boolean? = null,
    var e: E? = null
)
