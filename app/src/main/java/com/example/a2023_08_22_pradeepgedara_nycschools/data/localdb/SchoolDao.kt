package com.example.a2023_08_22_pradeepgedara_nycschools.data.localdb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SchoolDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchoolList(SchoolListEntity: List<SchoolListEntity>)

//    @Query("DELETE from SchoolListEntity")
//    suspend fun deleteSchoolList()

    @Query("SELECT * FROM SchoolListEntity")
    suspend fun getSchoolList(): List<SchoolListEntity>
    //
    @Query("SELECT * FROM SchoolListEntity WHERE dbn=:dbn")
    suspend fun getSchoolList(dbn: String): SchoolListEntity?
}