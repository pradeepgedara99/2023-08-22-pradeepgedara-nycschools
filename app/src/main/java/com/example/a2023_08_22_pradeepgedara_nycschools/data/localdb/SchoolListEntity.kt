package com.example.a2023_08_22_pradeepgedara_nycschools.data.localdb

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.a2023_08_22_pradeepgedara_nycschools.models.SchoolListDtoItem

@Entity
data class SchoolListEntity(
    @PrimaryKey val dbn: String,
    val schoolName: String?,
    val overView: String?,
    val neighborhood: String?,
    val phoneNumber: String?,
    val faxNumber: String?,
    val schoolEmail: String?,
    val schoolWebsite: String?,
    val primaryAddress: String?,
    val city: String?,
    val zip: String?,
    val stateCode: String?
) {
    companion object {
        fun fromDto(dto: SchoolListDtoItem): SchoolListEntity {
            return SchoolListEntity(
                dbn = dto.dbn ?: "",
                schoolName = dto.schoolName,
                overView = dto.overView,
                neighborhood = dto.neighborhood,
                phoneNumber = dto.phoneNumber,
                faxNumber = dto.faxNumber,
                schoolEmail = dto.schoolEmail,
                schoolWebsite = dto.schoolWebsite,
                primaryAddress = dto.primaryAddress,
                city = dto.city,
                zip = dto.zip,
                stateCode = dto.stateCode
            )
        }
    }

    fun toSchool(): SchoolListDtoItem {
        return SchoolListDtoItem(
            dbn = dbn,
            schoolName = schoolName,
            overView = overView,
            neighborhood = neighborhood,
            phoneNumber = phoneNumber,
            faxNumber = faxNumber,
            schoolEmail = schoolEmail,
            schoolWebsite = schoolWebsite,
            primaryAddress = primaryAddress,
            city = city,
            zip = zip,
            stateCode = stateCode
        )
    }
}


