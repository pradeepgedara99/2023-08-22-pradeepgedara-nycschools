package com.example.a2023_08_22_pradeepgedara_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApplication : Application(){

}