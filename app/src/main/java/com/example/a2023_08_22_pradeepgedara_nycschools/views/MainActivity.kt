package com.example.a2023_08_22_pradeepgedara_nycschools.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a2023_08_22_pradeepgedara_nycschools.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}