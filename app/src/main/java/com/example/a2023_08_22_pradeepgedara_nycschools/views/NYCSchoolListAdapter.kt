package com.example.a2023_08_22_pradeepgedara_nycschools.views

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.a2023_08_22_pradeepgedara_nycschools.R
import com.example.a2023_08_22_pradeepgedara_nycschools.databinding.ItemSchoolBinding
import com.example.a2023_08_22_pradeepgedara_nycschools.models.SchoolListDtoItem

class NYCSchoolListAdapter(dataModelList: List<SchoolListDtoItem>, ctx: Context,
                           private val onClick :((dtoItem: SchoolListDtoItem) -> Unit)? = null):
    RecyclerView.Adapter<NYCSchoolListAdapter.ViewHolder>(){
    private var dataModelList: List<SchoolListDtoItem>
    private val context: Context

    init {
        this.dataModelList = dataModelList
        context = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemSchoolBinding>(inflater,
            R.layout.item_school, parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel: SchoolListDtoItem = dataModelList[position]
        bind(holder, dataModel)
    }

    private fun bind(holder: ViewHolder, dataModel: SchoolListDtoItem) {
        holder.itemRowBinding.apply {
            textViewSchoolName.text = dataModel.schoolName
            textViewSchoolAddress.text = dataModel.primaryAddress + dataModel.city + dataModel.zip
            textViewEmail.text = dataModel.schoolEmail
            textViewPhoneNumber.text = dataModel.phoneNumber
            schoolCardId.setOnClickListener {
                onClick?.let { it1 -> it1(dataModel) }
            }
        }

    }

    override fun getItemCount(): Int {
        Log.d("SCHOOL_LIST_SIZE", "getItemCount: ${dataModelList.size}")
        return dataModelList.size

    }

    class ViewHolder(val itemRowBinding: ItemSchoolBinding) :
        RecyclerView.ViewHolder(itemRowBinding.root) {


    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(newList: List<SchoolListDtoItem>) {
        dataModelList = newList
        notifyDataSetChanged()
    }

}