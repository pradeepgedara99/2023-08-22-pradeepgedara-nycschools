package com.example.a2023_08_22_pradeepgedara_nycschools.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a2023_08_22_pradeepgedara_nycschools.R
import com.example.a2023_08_22_pradeepgedara_nycschools.databinding.FragmentNYCSchoolListBinding
import com.example.a2023_08_22_pradeepgedara_nycschools.viewmodels.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class NYCSchoolListFragment : Fragment() {
    private lateinit var binding: FragmentNYCSchoolListBinding
    private var adapter: NYCSchoolListAdapter? = null
    private val schoolViewModel : SchoolViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentNYCSchoolListBinding.inflate(inflater,container,false)
        adapter = NYCSchoolListAdapter(ArrayList(), requireContext(), onClick = { it ->
            val action = NYCSchoolListFragmentDirections.actionNYCSchoolListFragmentToNYCSchoolDetailsFragment(it)
            findNavController().navigate(action)
        })
        observeData(schoolViewModel)

        return binding.root
    }

    /**
     * observeData method is observe data from Room database using Data Access Object and setting to UI with adapter
     */

    private fun observeData(schoolViewModel: SchoolViewModel){

        lifecycleScope.launch {
            schoolViewModel.schoolListStateFlow.collect{dataOrException ->
                when {
                    dataOrException.loading == true -> {
                        binding.progressBarSchoolList.visibility = View.VISIBLE
                    }
                    dataOrException.data != null -> {
                        // Handle data state (e.g. update UI with data)
                        adapter!!.updateList(dataOrException.data!!)
                        binding.progressBarSchoolList.visibility = View.GONE
                    }
                    dataOrException.e != null -> {
                        Toast.makeText(context,"Sorry Something wrong from our end when loading data",
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }
        binding.recyclerViewSchool.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerViewSchool.adapter = adapter

    }

}