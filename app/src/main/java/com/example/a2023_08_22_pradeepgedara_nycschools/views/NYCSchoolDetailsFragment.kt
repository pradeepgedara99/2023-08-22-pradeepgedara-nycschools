package com.example.a2023_08_22_pradeepgedara_nycschools.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.a2023_08_22_pradeepgedara_nycschools.R
import com.example.a2023_08_22_pradeepgedara_nycschools.databinding.FragmentNYCSchoolDetailsBinding
import com.example.a2023_08_22_pradeepgedara_nycschools.viewmodels.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class NYCSchoolDetailsFragment : Fragment() {
    private lateinit var binding: FragmentNYCSchoolDetailsBinding
    private val args : NYCSchoolDetailsFragmentArgs by navArgs()
    private val schoolViewModel : SchoolViewModel by viewModels()


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentNYCSchoolDetailsBinding.inflate(inflater, container, false)
        args.dto.dbn?.let { schoolViewModel.getSchoolInformation(it) }
        observeData(schoolViewModel)
        return binding.root
    }

    /**
     * observeData method is observe data from Room database using Data Access Object
     * filtering the scores response and setting to UI
     */

    private fun observeData(schoolViewModel: SchoolViewModel){
        lifecycleScope.launch {
            schoolViewModel.schoolInfoStateFlow.collect{ dataOrException ->
                when {
                    dataOrException.loading == true -> {
                        binding.progressBarDetails.visibility = View.VISIBLE
                    }
                    //setting from api not room
                    dataOrException.data != null -> {
                        val schoolInfo = dataOrException.data
                        binding.apply {
                            tvHeadingSchoolName.text = args.dto.schoolName
                            tvSchoolAddressDetail.text = args.dto.primaryAddress
                            tvSchoolEmailDetail.text = args.dto.schoolEmail
                            tvSchoolPhoneDetail.text = args.dto.phoneNumber
                            tvSchoolWebUrlDetail.text = args.dto.schoolWebsite
                            tvSchoolDetailsPara.text = args.dto.overView
                            tvReadingScore.text = schoolInfo?.satCriticalReadingAvgScore
                            tvScoreWriting.text = schoolInfo?.satWritingAvgScore
                            tvMathScore.text = schoolInfo?.satMathAvgScore
                            progressBarDetails.visibility = View.GONE
                        }
                    }
                    dataOrException.e != null -> {
                        Toast.makeText(context,"Sorry Something wrong from our end", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }

    }



}


